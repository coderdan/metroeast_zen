<?php
/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function metroeast_zen_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL)  {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  // Create the form using Forms API: http://api.drupal.org/api/7

  // Remove some of the base theme's settings.
  unset($form['themedev']['zen_wireframes']); // We don't need to toggle wireframes on this site.

  // toggle off the slogan
  $form['theme_settings']['toggle_slogan']['#default_value'] = 0;

  // toggle off the sit ename
  $form['theme_settings']['toggle_name']['#default_value'] = 0;

  // turn off rebuilding theme registry on every page
  $form['themedev']['zen_rebuild_registry']['#default_value'] = 0;

  // We are editing the $form in place, so we don't need to return anything.
}
