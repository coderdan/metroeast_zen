<?php

// Plugin definition
$plugin = array(
  'title' => t('MetroEast custom three column brick'),
  'category' => t('Columns: 3'),
  'icon' => 'threecol_me_brick.png',
  'theme' => 'threecol_me_brick',
  'css' => 'threecol_me_brick.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side'),
    'bottom' => t('Bottom'),
  ),
);
